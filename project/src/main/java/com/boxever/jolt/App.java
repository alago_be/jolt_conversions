/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package com.boxever.jolt;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public class App {


    public static void main(String[] args) {

        String transform = args[0];
        String input = args[1];
        Boolean pretty = args.length == 3? new Boolean(args[2]):false;


        List chainrSpecJSON = JsonUtils.filepathToList( transform );
        Chainr chainr = Chainr.fromSpec( chainrSpecJSON );

        Function<Object, String> printer = pretty ? JsonUtils::toPrettyJsonString : JsonUtils::toJsonString;

        //List inputJSON = JsonUtils.filepathToList( input );
        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(input))) {

            stream.forEach( l -> {
                Object jsonLine = JsonUtils.jsonToObject(l);
                Object transformedOutput = chainr.transform( jsonLine );
                System.out.println( printer.apply( transformedOutput ) );
            }
            );

        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
